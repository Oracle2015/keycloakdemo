package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

/**
 * Project name My Application
 * Created by ETINGE MABIAN
 * GitHub Handler = https://github.com/Ora-Kool
 * Email = etingemabian@gmail.com
 * File created on 10/23/22 @ 12:48 AM
 **/
public class AccessToken {
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("expires_in")
    private String expiresIn;
    @SerializedName("refresh_expires_in")
    private String refreshExpiresIn;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("id_token")
    private String idToken;
    @SerializedName("not-before-policy")
    private String notBeforePolicy;
    @SerializedName("session_state")
    private String sessionState;
    @SerializedName("scope")
    private String scope;

    public AccessToken(String accessToken, String expiresIn) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
    }
}
