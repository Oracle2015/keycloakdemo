package com.example.myapplication;

import android.app.Application;

/**
 * Project name My Application
 * Created by ETINGE MABIAN
 * GitHub Handler = https://github.com/Ora-Kool
 * Email = etingemabian@gmail.com
 * File created on 10/23/22 @ 8:53 AM
 **/
public class AppShell extends Application {
    private static AppShell instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static AppShell getInstance(){
        return instance;
    }
}
