package com.example.myapplication;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Project name My Application
 * Created by ETINGE MABIAN
 * GitHub Handler = https://github.com/Ora-Kool
 * Email = etingemabian@gmail.com
 * File created on 10/23/22 @ 12:42 AM
 **/
public interface GetDataService {

    @FormUrlEncoded
    @POST("auth/realms/protocal/openid-connect/token")
    Call<AccessToken> getAccessToken(
            @Field("client_id") String client_id,
            @Field("grant_type") String grant_type,
            @Field("client_secret") String client_secret,
            @Field("scope") String scooe,
            @Field("username") String username,
            @Field("password") String password

    );
}
