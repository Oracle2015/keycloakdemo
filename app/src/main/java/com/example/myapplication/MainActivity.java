package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.databinding.ActivityMainBinding;
import com.google.gson.Gson;

import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.EndSessionRequest;
import net.openid.appauth.ResponseTypeValues;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityMainBinding binding;
    AuthorizationServiceConfiguration serviceConfig;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.signAuth.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        serviceConfig =
                new AuthorizationServiceConfiguration(
                        Uri.parse("https://keycloak-staging.drimzwallet.com/auth/realms/master/protocol/openid-connect/auth"), // authorization endpoint
                        Uri.parse("https://keycloak-staging.drimzwallet.com/auth/realms/master/protocol/openid-connect/token")); // token endpoint


        AuthorizationRequest.Builder authRequestBuilder =
                new AuthorizationRequest.Builder(
                        serviceConfig, // the authorization service configuration
                        "drimzwallet", // the client ID, typically pre-registered and static
                        ResponseTypeValues.CODE, // the response_type value: we want a code
                        Uri.parse("com.example.myapplication:/oauth2callback")); // the redirect URI to which the auth response is sent


        authRequestBuilder.setScope("profile");
        AuthorizationService authService = new AuthorizationService(this);
        Intent authIntent = authService.getAuthorizationRequestIntent(authRequestBuilder.build());
        startActivityForResult(authIntent, 1000);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            AuthorizationException ex = AuthorizationException.fromIntent(data);
            AuthorizationResponse resp = AuthorizationResponse.fromIntent(data);

            // ... process the response or exception ...
            if (ex == null){
                if (resp != null){

                }

            }
            Log.d("onActivity", new Gson().toJson(resp));

            Log.d("onActivity2", new Gson().toJson(ex));
            Log.d("onActivity", "found something but not sure");
        } else {
            Log.d("onActivity", "nothing found");
        }
    }
}