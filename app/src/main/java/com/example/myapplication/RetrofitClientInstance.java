package com.example.myapplication;

import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Project name My Application
 * Created by ETINGE MABIAN
 * GitHub Handler = https://github.com/Ora-Kool
 * Email = etingemabian@gmail.com
 * File created on 10/23/22 @ 12:37 AM
 **/
public class RetrofitClientInstance {
    private static Retrofit retrofit;
    private static String BASE_URL = "https://keycloak-staging.drimzwallet.com/auth/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }

        return retrofit;
    }


}
